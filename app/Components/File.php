<?php

namespace App\Components;

use DirectoryIterator;
use PharData;
use App\Entities\File as Entity;

class File
{
    /**
     * @param $dir
     * @param string $extension
     * @return Entity[]
     */
    public function getList($dir, $extension = 'gz')
    {
        if ($dir === false) {
            throw new \RuntimeException();
        }

        $iterator = new DirectoryIterator($dir);

        $result = [];

        foreach ($iterator as $value) {
            if ($value->isFile() && $value->getExtension() == $extension) {
                $entity = new Entity();
                $entity->setName($value->getBasename());
                $entity->setPath($value->getPathname());
                $result[] = $entity;
            }
        }
        return $result;
    }

    public function unpack(Entity $file, $dir)
    {
        try {
            $archive = new PharData($file->getPath());
            $tmp = $archive->decompress();
            $tmp->extractTo($dir, null, true);
            // PharData::unlinkArchive($tmp->getPathname());
            $filename = last(explode(DIRECTORY_SEPARATOR, $tmp->getPathname()));
            $file->setName($filename);
        } catch (\Exception $e) {

        }
    }
}