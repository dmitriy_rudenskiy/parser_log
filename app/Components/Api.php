<?php
namespace App\Components;


class Api
{
    /**
     * @param string $query
     * @return mixed|null
     */
    public function getFromQuery($query)
    {
        $url = 'http://dnor.ru/api/classification?' . http_build_query(['q' => $query]);
        $response = @file_get_contents($url);

        if (empty($response)) {
            return null;
        }

        $json = json_decode($response);
        return $json;
    }

    /**
     * @param int $id
     * @return mixed|null
     */
    public function getFromId($id)
    {
        $url = 'http://dnor.ru/api/find/' . $id;
        $response = @file_get_contents($url);

        if (empty($response)) {
            return null;
        }

        $json = json_decode($response);
        return $json;
    }
}