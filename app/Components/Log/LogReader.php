<?php

namespace App\Components\Log;

use InvalidArgumentException;

class LogReader
{
    private $handle;

    public function __construct($filename)
    {
        $filename = realpath($filename);

        if ($filename === false) {
            throw new InvalidArgumentException();
        }

        if (!is_readable($filename)) {
            throw new InvalidArgumentException();
        }

        $this->handle = fopen($filename, "r");
    }

    public function __destruct()
    {
        fclose($this->handle);
    }

    public function read()
    {
        while (($buffer = fgets($this->handle, 4096)) !== false) {
            yield $buffer;
        }
    }
}