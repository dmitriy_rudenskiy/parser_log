<?php

namespace App\Components\Log;

interface LogSource
{
    /**
     *
     */
    const SOURCE_SEARCH = 1;

    /**
     *
     */
    const SOURCE_VIEW = 2;

    /**
     *
     */
    const SOURCE_SUCCESS = 3;

    /**
     *
     */
    const SOURCE_NOT_FIND = 4;
}