<?php

namespace App\Components\Log;

class LogMapper
{
    const HTTP_CODE_SUCCESS = 200;

    /**
     * @param string $string
     * @return array|null
     */
    public function read($string)
    {
        $params = explode('"', $string);

        if (sizeof($params) < 5) {
            return null;
        }

        list($rawData, $rawUrl, $code, $refer,, $agent) = $params;

        // не удалось загрузить страницу
        if ($this->getCode($code) !== self::HTTP_CODE_SUCCESS) {
            return null;
        }

        // убираем ботов
        if (strpos(strtolower($agent), 'bot') !== false) {
            return null;
        }

        $result = [
            'dateTime' => $this->getDateTime($rawData),
            'ip' => $this->getIp($rawData),
            'agent' => $agent,
            'refer' => $this->getReferId($refer),
            'url' => $this->getUrl($rawUrl)
        ];

        return (object)$result;
    }

    protected function getIp($string)
    {
        return ip2long(explode(' ', $string)[0]);
    }

    protected function getUrl($string)
    {
        $url = explode(' ', $string)[1];

        parse_str(parse_url($url, PHP_URL_QUERY), $params);
        $params['path'] = parse_url($url, PHP_URL_PATH);

        return (object)$params;
    }

    protected function getCode($string)
    {
        $string = trim($string);
        $code = explode(' ', $string)[0];

        return (int)$code;
    }

    protected function getDateTime($string)
    {
        preg_match("/\[(.*)\]/", $string, $matches);

        return \DateTime::createFromFormat('d/M/Y:H:i:s P', $matches[1]);
    }

    protected function getReferId($url)
    {
        $tools = new LogRefer();
        return $tools->get($url);
    }
}