<?php

namespace App\Components\Log;

class LogRefer
{
    public function get($string)
    {
        if (empty($refer) || $refer == '-') {
            return 1;
        }

        if (strpos($refer, 'dnor.ru/search') !== false) {
            return 3;
        }

        if (strpos($refer, 'dnor.ru/simple/search') !== false) {
            return 4;
        }

        if (strpos($refer, 'dnor.ru/auto') !== false) {
            return 5;
        }

        if (strpos($refer, 'dnor.ru/zapchasty') !== false) {
            return 6;
        }

        // обновил страницу
        if (strpos($refer, 'dnor.ru/ad/part') !== false) {
            return 7;
        }

        if (strpos($refer, 'dnor.ru/ad/other-offers') !== false) {
            return 8;
        }

        // тестировал разработчик
        if (strpos($refer, 'dnor.ru:') !== false) {
            return 9;
        }

        if (strpos($refer, 'autodeviz.ru/') !== false) {
            return 10;
        }

        if (strpos($refer, 'yandex.ru/clck') !== false) {
            return 11;
        }

        if (strpos($refer, 'baidu.com') !== false) {
            return 12;
        }

        if (strpos($refer, 'yandex.net/yandbtm') !== false) {
            return 13;
        }

        if (strpos($refer, 'www.google.') !== false) {
            return 14;
        }

        // нашли на главной в списке
        if (strpos($refer, 'dnor.ru') !== false) {
            return 2;
        }

        return null;
    }
}