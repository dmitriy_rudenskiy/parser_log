<?php
namespace App\Console\Commands;

use App\Components\File;
use App\Entities\File as Entity;
use Illuminate\Console\Command;
use SplFileInfo;

class Sync extends Command
{
    protected $signature = 'log:sync:archive';

    public function handle()
    {
        $dir = realpath(env('ARCHIVE_FOLDER'));

        $tools = new File();
        $listFiles = $tools->getList($dir);

        foreach ($listFiles as $value) {
            $tools->unpack($value, $dir);
            $this->add($value);
        }
    }

    protected function add(Entity $value)
    {
        dd($value);
    }
}