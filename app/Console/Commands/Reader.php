<?php

namespace App\Console\Commands;

use App\Components\Api;
use App\Components\File;
use App\Components\Log\LogMapper;
use App\Components\Log\LogReader;
use App\Components\Log\LogSource;
use Illuminate\Console\Command;

class Reader extends Command implements LogSource
{
    protected $signature = 'log:read:file';

    /**
     * @var Api
     */
    private $api;

    public function handle()
    {
        $dir = realpath(env('ARCHIVE_FOLDER'));

        $this->api = new Api();

        $tools = new File();
        $listFiles = $tools->getList($dir, 'log');
        $mapper = new LogMapper();

        foreach ($listFiles as $file) {
            $reader = new LogReader($file->getPath());

            foreach ($reader->read() as $line) {
                $object = $mapper->read($line);

                if ($object !== null) {
                    //$this->check($object);
                }
            }
        }
    }

    protected function check($object)
    {
        $result = (object)[
            "sourceId" => 0,
            "makerId" => 0,
            "modelId" => 0,
            "partId" => 0
        ];
        // поиск
        if ($object->url->path == '/search') {

            // указаны параметры в поиске
            if (isset($object->url->_model) && isset($object->url->_model) && isset($object->url->_model)) {
                $result->sourseId = self::SOURCE_SEARCH;
                $result->makerId = (int)$object->url->_maker;
                $result->modelId = (int)$object->url->_model;
                $result->partId = (int)$object->url->_spare;

                return $result;
            }

            // полнотекстовый поиск
            if (isset($object->url->q)) {

                $data = $this->api->getFromQuery($object->url->q);

                if ($data !== null) {
                    $result->sourseId = self::SOURCE_SEARCH;
                    $result->makerId = (int)$data["maker_id"];
                    $result->modelId = (int)$data["model_id"];
                    $result->partId = (int)$data["part_id"];
                }

                return $result;
            }
        }

        // смотрели товары
        if (strpos($object->url->path, '/ad/part') !== false) {

            $productId = (int)str_replace('/ad/part/', '', $object->url->path);

            $data = $this->api->getFromId($productId);

            if ($data !== null) {
                $result->sourseId = self::SOURCE_VIEW;
                $result->makerId = (int)$data[0];
                $result->modelId = (int)$data[1];
            }

            return $result;
        }

        // успешные оформления
        if ($object->url->path == '/select/from/razbor/success') {
            if (isset($object->url->m)) {
                $result->sourseId = self::SOURCE_SUCCESS;
                $result->makerId = (int)$object->url->m;

                return $result;
            }
        }

        // не нашли
        if ($object->url->path == '/search/on/autodeviz') {
            if (isset($object->url->m)) {
                $result->sourseId = self::SOURCE_NOT_FIND;
                $result->makerId = (int)$object->url->m;

                return $result;
            }
        }
    }
}